# Testing in Java

Supporting examples for presentation: https://docs.google.com/presentation/d/1vgzsp2TssKpxDo0J-FuQhA8q5Fb7QBDbK9k1knHtr4Q

# Testing in Java (system tests)

Supporting examples for presentation: https://docs.google.com/presentation/d/1F77fA8ZBOiQredOzwl5w4We9-WTRDSWs8-pguhOCuBk

### How to run
System tests are not part of a regular test suite so these are excluded from standard run:
> mvn test

To run particular version of system test you have to run it specifically:
> mvn test -Dtest=SpockBasedSystemSpec|JBehaveBasedSystemTest|CucumberBasedSystemTest|

For Gatling load tests run, but have in mind the instance of application has to already run:
> mvn gatling:execute

For JMeter load tests, which is nicely attached to regular Maven lifecycle, run:
> mvn clean verify

For Locust load tests, after installing locust locally on the machine, run:
> locust --host=http://localhost:8081 --locustfile locustfile.py

and access Locust Dashboard at http://localhost:8089