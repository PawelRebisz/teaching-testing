from locust import HttpLocust, TaskSet, task, between
import json
import datetime

dt = datetime.datetime(2020, 1, 1)
step = datetime.timedelta(days = 1)

class UserBehavior(TaskSet):

    @task(1)
    def make_reservation(self):
        global dt, step
        dt = dt + step
        headers = {'content-type': 'application/json','Accept-Encoding':'gzip'}
        self.client.post("/reservations/", data = json.dumps({
          "name": "John",
          "surname": "Smith",
          "age": 25,
          "date": dt.strftime("%Y-%m-%d")
        }),
        headers = headers,
        name = "Make a reservation")

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(0.5, 1.0)
