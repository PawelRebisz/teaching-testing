package com.redbull.teaching.testing.service

import com.redbull.teaching.testing.domain.Person
import com.redbull.teaching.testing.domain.Reservation
import com.redbull.teaching.testing.repository.ReservationRepository
import spock.lang.Specification

import java.time.LocalDate

class DefaultReservationServiceSpec extends Specification {

    def PERSON = new Person("John", "Smith", 25)
    def DATE_OF_RESERVATION = LocalDate.of(2017, 4, 1)

    def reservationRepository = Mock(ReservationRepository)
    def roomAvailability = Mock(RoomAvailability)
    def invoiceService = Mock(InvoiceService)
    def reservationService = new DefaultReservationService(reservationRepository, roomAvailability, invoiceService)

    def "should accept the reservation from person on selected day"() {
        when:
            reservationService.makeReservation(PERSON, DATE_OF_RESERVATION)
        then:
            1 * reservationRepository.acceptReservation(_)
    }

    def "should return reservation for person"() {
        given:
            roomAvailability.reserveForDate(DATE_OF_RESERVATION) >> 15
        when:
            Reservation reservation = reservationService.makeReservation(PERSON, DATE_OF_RESERVATION)
        then:
            reservation.person.name == "John"
            reservation.person.surname == "Smith"
            reservation.roomNumber == 15
            reservation.date == DATE_OF_RESERVATION
    }
}
