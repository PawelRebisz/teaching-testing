package com.redbull.teaching.testing.system.spock

import com.redbull.teaching.testing.domain.Person
import org.json.JSONObject
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import spock.lang.Unroll

import static com.redbull.teaching.testing.system.tools.SystemTestHelper.templateWithH2Datasource

class SpockBasedSystemSpec extends Specification {

    private RestTemplate restTemplate =  new RestTemplate()
    private JdbcTemplate jdbcTemplate = templateWithH2Datasource()

    @Unroll
    def "reservation for person with age #age should cost #expectedPrice"(String name, String surname,
                                                                          int age, String dateAsString, int expectedPrice) {
        given:
            Person person = new Person(name, surname, age)
        when:
            JSONObject json = person.toJson()
            json.put("date", dateAsString)
            HttpHeaders headers = new HttpHeaders()
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8)
            HttpEntity<String> request = new HttpEntity<String>(json.toString(), headers)
            String response = restTemplate
                    .postForObject("http://localhost:8081/reservations/", request, String.class)

            String reservationId = new JSONObject(response)
                    .getString("reservationId")
        then:
            String sql = String.format("SELECT price FROM reservations WHERE id = '%s' ", reservationId)
            int amountOfEntries = jdbcTemplate.queryForObject(sql, Integer.class)
            amountOfEntries == expectedPrice
        where:
            name   | surname   | age | dateAsString   | expectedPrice
            'John' | 'Smith'   | 17  | '2017-11-10'   | 100
            'Bob'  | 'White'   | 25  | '2017-11-11'   | 250
            'Nick' | 'Brown'   | 66  | '2017-11-12'   | 150
    }
}
