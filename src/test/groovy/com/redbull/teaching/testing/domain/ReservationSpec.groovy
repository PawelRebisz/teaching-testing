package com.redbull.teaching.testing.domain

import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate

class ReservationSpec extends Specification {

    @Unroll
    def "reservation for person with age #age should cost #expectedPrice"(int age, int expectedPrice) {
        given:
            Person person = personWithAge(age)
        when:
            Reservation reservation = new Reservation(person, 1, LocalDate.now())
        then:
            reservation.price == expectedPrice
        where:
            age | expectedPrice
            15  | 100
            34  | 250
            62  | 150
    }

    private Person personWithAge(int age) {
        return new Person(
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            age
        );
    }
}
