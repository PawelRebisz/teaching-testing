package com.redbull.teaching.testing.system.gatling

import java.util.concurrent.{ThreadLocalRandom}

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.core.scenario.Simulation

class GatlingBasedSystemTest extends Simulation {
  val random = ThreadLocalRandom.current
  val configuraion = http.baseUrl("http://localhost:8081")

  var current = java.time.LocalDate.now
  val count = new java.util.concurrent.atomic.AtomicInteger(0)

  def increment(counter: java.util.concurrent.atomic.AtomicInteger) : Int = {
    return counter.getAndIncrement()
  }

  val reservationScenario = scenario("Performance testing of reservation service")
    .repeat(100, "n") {
      exec(
        http("Make a reservation")
          .post("/reservations/")
          .header("Content-Type", "application/json")
          .body(StringBody(session => "{\"name\":\"John\", \"surname\":\"Smith\", \"age\":25, \"date\":\"" + current.plusDays(increment(count)) + "\"}"))
          .check(status.is(200))
      )
    }
    setUp(reservationScenario.inject(atOnceUsers(5)))
                      .protocols(configuraion)
                      .assertions(
                        global.responseTime.max.lt(500),
                        global.responseTime.mean.lt(1000),
                        global.successfulRequests.percent.gt(95)
                      )
}