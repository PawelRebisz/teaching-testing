Narrative:
An application is required to accept reservation from various people

Scenario: Person with various ages
Given a person <name> <surname> with age <age>
When decides to make a reservation at <date>
Then system accepts the reservation for <price>

Examples:
| name | surname | age | date       | price |
| John | Smith   | 17  | 2017-11-10 | 100   |
| Bob  | White   | 25  | 2017-11-11 | 250   |
| Nick | Brown   | 66  | 2017-11-12 | 150   |