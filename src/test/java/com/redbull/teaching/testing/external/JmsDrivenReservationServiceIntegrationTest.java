package com.redbull.teaching.testing.external;

import com.redbull.teaching.testing.configuration.JmsDrivenReservationServiceConfiguration;
import com.redbull.teaching.testing.configuration.TestableJmsBroker;
import com.redbull.teaching.testing.configuration.TestableReservationServiceConfiguration;
import com.redbull.teaching.testing.service.DummyReservationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.jms.MapMessage;
import javax.jms.Queue;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

import static com.jayway.awaitility.Awaitility.await;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {JmsDrivenReservationServiceConfiguration.class, TestableReservationServiceConfiguration.class, TestableJmsBroker.class})
public class JmsDrivenReservationServiceIntegrationTest {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Queue queue;

    @Autowired
    private DummyReservationService reservationService;

    @Test
    public void shouldMakeAReservationOnReservationEvent() throws Exception  {
        // given
        LocalDate expectedDate = LocalDate.of(2017, 4, 1);

        // when
        sendMessage("John", "Smith", 25, "2017-04-01");

        // then
        await()
            .atMost(2, TimeUnit.SECONDS)
            .pollInterval(200, TimeUnit.MILLISECONDS)
            .until(() -> {
                System.out.println("Checking if message was passed to reservation service.");
                return expectedDate.equals(reservationService.getLastDate());
            });
    }

    private void sendMessage(final String name, final String surname, final int age, final String date) {
        jmsTemplate.send(this.queue, session -> {
            MapMessage message = session.createMapMessage();
            message.setString("name", name);
            message.setString("surname", surname);
            message.setInt("age", age);
            message.setString("date", date);
            return message;
        });
    }
}