package com.redbull.teaching.testing.external;

import com.redbull.teaching.testing.configuration.RestDrivenReservationServiceConfiguration;
import com.redbull.teaching.testing.configuration.TestableReservationServiceConfiguration;
import com.redbull.teaching.testing.service.DummyReservationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;

import static com.redbull.teaching.testing.matchers.UuidMatcher.isUuid;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {RestDrivenReservationServiceConfiguration.class, TestableReservationServiceConfiguration.class})
@WebAppConfiguration
public class RestDrivenReservationServiceIntegrationTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext ctx;

    @Autowired
    private DummyReservationService reservationService;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
    }

    @Test
    public void shouldMakeAReservationUsingExposedEndpoint() throws Exception  {
        // expect
        mockMvc
            .perform(post("/reservations/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"John\", \"surname\":\"Smith\", \"age\":25, \"date\":\"2017-04-01\"}")
            )
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("$.reservationId").value(isUuid()));

        // and
        assertThat(reservationService.getLastDate(), equalTo(LocalDate.of(2017, 4, 1)));
    }
}