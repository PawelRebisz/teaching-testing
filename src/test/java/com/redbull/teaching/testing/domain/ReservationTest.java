package com.redbull.teaching.testing.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.UUID;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ReservationTest {

    @ParameterizedTest(name = "Reservation for person with age {0} should cost {1}")
    @MethodSource("data")
    public void shouldCalculateDifferentPriceForDifferentGuestsDependingOnTheAge(int age, int expectedPrice) {
        // given
        Person person = personWithAge(age);

        // when
        Reservation reservation = new Reservation(person, 1, LocalDate.now());

        // then
        assertThat(reservation.getPrice(), equalTo(expectedPrice));
    }

    private static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of(15, 100),
                Arguments.of(34, 250),
                Arguments.of(62, 150)
        );
    }

    private static Person personWithAge(int age) {
        return new Person(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                age
        );
    }
}