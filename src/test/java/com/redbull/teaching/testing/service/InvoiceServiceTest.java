package com.redbull.teaching.testing.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.jayway.awaitility.Awaitility.await;

public class InvoiceServiceTest {

    @TempDir
    public Path temporaryFolder;

    private Path invoiceDirectory;
    private InvoiceService invoiceService;

    @BeforeEach
    public void setUp() throws Exception {
        invoiceDirectory = temporaryFolder.resolve("invoices");
        invoiceService = new InvoiceService(invoiceDirectory);
    }

    @DisplayName("The service should generate an invoice as a file on the hard drive.")
    @Test
    public void shouldGenerateAFileWithAnInvoice() {
        // given
        UUID reservationId = UUID.randomUUID();

        // when
        invoiceService.generateInvoice(reservationId);

        // then
        await()
                .atMost(4, TimeUnit.SECONDS)
                .pollInterval(200, TimeUnit.MILLISECONDS)
                .until(() -> {
                    System.out.println("Checking if invoice was already created.");
                    return Files.exists(invoiceDirectory.resolve(reservationId + ".txt"));
                });
    }
}