package com.redbull.teaching.testing.service;

import com.redbull.teaching.testing.domain.Person;
import com.redbull.teaching.testing.domain.Reservation;
import com.redbull.teaching.testing.domain.ReservationRefusedException;

import java.io.IOException;
import java.time.LocalDate;

public class DummyReservationService implements ReservationService {

    private Person lastPerson;
    private LocalDate lastDate;

    @Override
    public Reservation makeReservation(Person person, LocalDate date) throws ReservationRefusedException, IOException {
        lastPerson = person;
        lastDate = date;
        return new Reservation(person, 1, date);
    }

    public Person getLastPerson() {
        return lastPerson;
    }

    public LocalDate getLastDate() {
        return lastDate;
    }
}
