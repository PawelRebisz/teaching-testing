package com.redbull.teaching.testing.service;

import com.redbull.teaching.testing.domain.Person;
import com.redbull.teaching.testing.domain.Reservation;
import com.redbull.teaching.testing.domain.ReservationRefusedException;
import com.redbull.teaching.testing.repository.ReservationRepository;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;

import static com.redbull.teaching.testing.matchers.ReservationMatcher.aReservation;
import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

public class DefaultReservationServiceTest {

    private static final Person PERSON = new Person("John", "Smith", 25);
    private static final LocalDate DATE_OF_RESERVATION = LocalDate.of(2017, 4, 1);

    private final ReservationRepository reservationRepository = mock(ReservationRepository.class);
    private final RoomAvailability roomAvailability = mock(RoomAvailability.class);
    private final InvoiceService invoiceService = mock(InvoiceService.class);
    private final ReservationService reservationService = new DefaultReservationService(reservationRepository, roomAvailability, invoiceService);

    @Test
    public void shouldAcceptTheReservationFromPersonOnSelectedDay() throws ReservationRefusedException, IOException {
        // when
        reservationService.makeReservation(PERSON, DATE_OF_RESERVATION);

        // then
        verify(reservationRepository, times(1)).acceptReservation(argThat(any(Reservation.class)));
    }

        @Test
    public void shouldReturnReservationForPerson() throws ReservationRefusedException, IOException {
        // given
        int roomNumber = 5;
        given(roomAvailability.reserveForDate(anyObject())).willReturn(roomNumber);

        // when
        Reservation reservation = reservationService.makeReservation(PERSON, DATE_OF_RESERVATION);

        // non-matcher
        assertEquals(reservation.getPerson().getName(), "John");
        assertEquals(reservation.getPerson().getSurname(), "Smith");
        assertEquals(reservation.getPerson().getAge(), 25);
        assertEquals(reservation.getRoomNumber(), roomNumber);

        // matcher
        assertThat(reservation, aReservation().withName("John").withSurname("Smith").withAge(25).withRoomNumber(roomNumber).build());
    }
}