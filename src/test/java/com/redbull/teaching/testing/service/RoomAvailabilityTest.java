package com.redbull.teaching.testing.service;

import com.redbull.teaching.testing.domain.ReservationRefusedException;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RoomAvailabilityTest {

    private final RoomAvailability roomAvailability = new RoomAvailability(1, 2);

    @Test
    public void shouldBlowUpIfNoRoomIsAvailableForGivenDate() {
        ReservationRefusedException expectedException = assertThrows(ReservationRefusedException.class,
            () -> {
                // when
                roomAvailability.reserveForDate(LocalDate.of(2017, 4, 1));
                roomAvailability.reserveForDate(LocalDate.of(2017, 4, 1));
                roomAvailability.reserveForDate(LocalDate.of(2017, 4, 1));

            }
        );
        assertThat(expectedException.getMessage(), equalTo("No available rooms for date 2017-04-01."));
    }

    @Test
    public void shouldAssignDifferentRoomNumbersForConsequentRoomReservations() throws ReservationRefusedException {
        // when
        int roomNumber = roomAvailability.reserveForDate(LocalDate.of(2017, 4, 1));
        int anotherRoomNumber = roomAvailability.reserveForDate(LocalDate.of(2017, 4, 1));

        // then
        assertThat(roomNumber, not(equalTo(anotherRoomNumber)));
    }
}