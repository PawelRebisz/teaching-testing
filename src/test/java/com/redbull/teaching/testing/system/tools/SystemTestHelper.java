package com.redbull.teaching.testing.system.tools;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.sql.Driver;

public class SystemTestHelper {
	public static JdbcTemplate templateWithH2Datasource() {
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		try {
			dataSource.setDriverClass((Class<? extends Driver>) Class.forName("org.h2.Driver"));
		} catch (ClassNotFoundException exception) {
			exception.printStackTrace();
		}
		dataSource.setUrl("jdbc:h2:tcp://localhost:9090/mem:testdb");
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		return new JdbcTemplate(dataSource);
	}
}
