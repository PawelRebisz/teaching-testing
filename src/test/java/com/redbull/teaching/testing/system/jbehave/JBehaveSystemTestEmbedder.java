package com.redbull.teaching.testing.system.jbehave;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.EmbedderControls;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.parsers.RegexPrefixCapturingPatternParser;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.reporters.SurefireReporter;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.PrintStreamStepMonitor;

import java.util.Properties;

import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;
import static org.jbehave.core.reporters.Format.CONSOLE;
import static org.jbehave.core.reporters.Format.HTML;

public class JBehaveSystemTestEmbedder extends Embedder {

    @Override
    public EmbedderControls embedderControls() {
        return new EmbedderControls()
                .doIgnoreFailureInStories(false)
                .doIgnoreFailureInView(false);
    }

    @Override
    public Configuration configuration() {
        Properties viewResources = new Properties();
        viewResources.put("decorateNonHtml","false");

        Class<? extends JBehaveSystemTestEmbedder> embedderClass = this.getClass();
        return new MostUsefulConfiguration()
                .useStoryLoader(new LoadFromClasspath(embedderClass.getClassLoader()))
                .useStoryReporterBuilder(
                    new StoryReporterBuilder()
                        .withViewResources(viewResources)
                        .withCodeLocation(codeLocationFromClass(embedderClass))
                        .withDefaultFormats()
                        .withSurefireReporter(new SurefireReporter(embedderClass))
                        .withFormats(CONSOLE, HTML)
                        .withFailureTrace(true).withFailureTraceCompression(true)
                        .withCrossReference(new CrossReference())
                )
                .useStepPatternParser(new RegexPrefixCapturingPatternParser("%"))
                .useStepMonitor(new PrintStreamStepMonitor())
                .doDryRun(false);
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new JBehaveSteps());
    }
}
