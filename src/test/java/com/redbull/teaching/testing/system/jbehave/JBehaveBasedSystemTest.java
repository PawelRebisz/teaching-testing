package com.redbull.teaching.testing.system.jbehave;

import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.StoryFinder;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

public class JBehaveBasedSystemTest {

    @Test
    void runJBehaveSystemTests() {
        Embedder embedder = new JBehaveSystemTestEmbedder();
        List<String> storyPaths = new StoryFinder()
                .findPaths(codeLocationFromClass(this.getClass()), "system/jbehave/*.story", "");
        embedder.runStoriesAsPaths(storyPaths);
    }
}
