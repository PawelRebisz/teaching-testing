package com.redbull.teaching.testing.system.jbehave;

import com.redbull.teaching.testing.domain.Person;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static com.redbull.teaching.testing.system.tools.SystemTestHelper.templateWithH2Datasource;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class JBehaveSteps {
    private RestTemplate restTemplate =  new RestTemplate();
    private JdbcTemplate jdbcTemplate = templateWithH2Datasource();

    private Person person;
    private String dateAsString;
    private String reservationId;

    @Given("a person %name %surname with age %age")
    public void givenPerson(@Named("name") String name,
                            @Named("surname") String surname,
                            @Named("age") int age) {
        person = new Person(name, surname, age);
    }

    @When("decides to make a reservation at %date")
    public void whenMakesAReservation(@Named("date") String date) throws IOException {
        dateAsString = date;

        JSONObject json = person.toJson();
        json.put("date", dateAsString);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> request = new HttpEntity<String>(json.toString(), headers);
        String response = restTemplate
				.postForObject("http://localhost:8081/reservations/", request, String.class);

        reservationId = new JSONObject(response)
				.getString("reservationId");
    }

    @Then("system accepts the reservation for %price")
    public void thenSystemShouldHaveIt(@Named("price") int price) {
        String sql = String.format(
                "SELECT price FROM reservations WHERE id = '%s' ", reservationId);
        int amountOfEntries = jdbcTemplate.queryForObject(sql, Integer.class);
        assertThat(amountOfEntries, equalTo( price));
    }
}