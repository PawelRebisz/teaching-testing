package com.redbull.teaching.testing.system.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;

@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"pretty", "html:target/cucumber"},
    glue = {"com.redbull.teaching.testing.system.cucumber"},
    features = "src/test/resources/system/cucumber",
    snippets = CAMELCASE
)
public class CucumberBasedSystemTest {

}