package com.redbull.teaching.testing.repository;

import com.redbull.teaching.testing.configuration.TestableDataSource;
import com.redbull.teaching.testing.domain.Person;
import com.redbull.teaching.testing.domain.Reservation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestableDataSource.class})
public class DefaultReservationRepositoryIntegrationTest {

    private ReservationRepository reservationRepository;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;

    @BeforeEach
    public void setUp() {
        reservationRepository = new DefaultReservationRepository(dataSource);
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void shouldStoreAcceptedReservation() throws Exception {
        // given
        LocalDate date = LocalDate.of(2017, 4, 1);
        Reservation reservation = new Reservation(new Person("John", "Smith", 25), 25, date);
        UUID id = reservation.getId();

        // when
        reservationRepository.acceptReservation(reservation);

        // then
        String sql = String.format(
                "SELECT count(id) FROM reservations WHERE id = '%s' AND reservation_on = '%s'", id, date.toString());
        long amountOfEntries = jdbcTemplate.queryForObject(sql, Long.class);
        assertThat(amountOfEntries, equalTo(1L));
    }
}