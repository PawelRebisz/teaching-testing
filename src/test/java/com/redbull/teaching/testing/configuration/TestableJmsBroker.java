package com.redbull.teaching.testing.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.*;

@Configuration
public class TestableJmsBroker {

    @Bean
    public ConnectionFactory connectionFactory() throws JMSException {
        return new ActiveMQConnectionFactory("vm://localhost?broker.persistent=false");
    }

    @Bean
    public Destination jmsBroker(ConnectionFactory connectionFactory) throws JMSException {
        QueueConnection queueConn = (QueueConnection) connectionFactory.createConnection();
        QueueSession queueSession = queueConn.createQueueSession(false, Session.DUPS_OK_ACKNOWLEDGE);
        return queueSession.createQueue("reservations");
    }

    @Bean
    public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {
        return new JmsTemplate(connectionFactory);
    }
}
