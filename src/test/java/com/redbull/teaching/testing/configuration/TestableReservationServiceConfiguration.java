package com.redbull.teaching.testing.configuration;

import com.redbull.teaching.testing.service.ReservationService;
import com.redbull.teaching.testing.service.DummyReservationService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestableReservationServiceConfiguration {

    @Bean
    public ReservationService reservationService() {
        return new DummyReservationService();
    }
}
