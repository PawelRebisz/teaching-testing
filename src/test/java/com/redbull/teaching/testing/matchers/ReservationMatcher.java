package com.redbull.teaching.testing.matchers;

import com.redbull.teaching.testing.domain.Reservation;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class ReservationMatcher extends TypeSafeDiagnosingMatcher<Reservation> {

    private final String name;
    private final String surname;
    private final int age;
    private final int roomNumber;

    protected boolean matchesSafely(Reservation reservation, Description mismatchDescription) {
        boolean result = true;
        if(name != null && !name.equals(reservation.getPerson().getName())) {
            mismatchDescription.appendText("name is different ")
                .appendValue(reservation.getPerson().getName())
                .appendText(" than expected ").appendValue(name);
            result = false;
        }
        if(surname != null && !surname.equals(reservation.getPerson().getSurname())) {
            if(!result) {
                mismatchDescription.appendText(", ");
            }
            mismatchDescription.appendText("surname is different ")
                .appendValue(reservation.getPerson().getName())
                .appendText(" than expected ").appendValue(surname);
            result = false;
        }
        if(age != 0 && age != reservation.getPerson().getAge()) {
            if(!result) {
                mismatchDescription.appendText(", ");
            }
            mismatchDescription.appendText("age is different ")
                .appendValue(reservation.getPerson().getAge())
                .appendText(" than expected ").appendValue(age);
            result = false;
        }
        if(roomNumber != 0 && roomNumber != reservation.getRoomNumber()) {
            if(!result) {
                mismatchDescription.appendText(", ");
            }
            mismatchDescription.appendText("roomNumber is different ")
                .appendValue(reservation.getRoomNumber())
                .appendText(" than expected ").appendValue(roomNumber);
            result = false;
        }
        return result;
    }

    public void describeTo(Description description) {
        description.appendText("reservation with (");
        if(name != null) {
            description.appendText(" name: ").appendValue(name);
        }
        if(surname != null) {
            description.appendText(" surname:").appendValue(surname);
        }
        if(age != 0) {
            description.appendText(" age:").appendValue(age);
        }
        if(roomNumber != 0) {
            description.appendText(" roomNumber:").appendValue(roomNumber);
        }
        description.appendText(" )");
    }

    private ReservationMatcher(String name, String surname, int age, int roomNumber) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.roomNumber = roomNumber;
    }

    public static ReservationMatcherBuilder aReservation() {
        return new ReservationMatcherBuilder();
    }

    public static class ReservationMatcherBuilder {
        private final String DEFAULT = null;

        private String name = DEFAULT;
        private String surname = DEFAULT;
        private int age = 0;
        private int roomNumber = 0;

        public ReservationMatcherBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ReservationMatcherBuilder withSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public ReservationMatcherBuilder withAge(int age) {
            this.age = age;
            return this;
        }

        public ReservationMatcherBuilder withRoomNumber(int roomNumber) {
            this.roomNumber = roomNumber;
            return this;
        }

        public ReservationMatcher build() {
            return new ReservationMatcher(name, surname, age, roomNumber);
        }
    }
}
