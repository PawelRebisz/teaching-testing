package com.redbull.teaching.testing.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class UuidMatcher extends TypeSafeDiagnosingMatcher<String> {
    private static final String UUID_PATTERN = "[0-9a-fA-F]{8}(?:-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12}";

    public static Matcher<String> isUuid() {
        return new UuidMatcher();
    }

    @Override
    protected boolean matchesSafely(String value, Description mismatchDescription) {
        boolean result = value.matches(UUID_PATTERN);
        if(!result) {
            mismatchDescription
                .appendText("value ")
                .appendValue(value)
                .appendText(" does not fall into the uuid pattern");
        }
        return result;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a value which matches the uuid format");
    }
}
