package com.redbull.teaching.testing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.redbull.teaching.testing.configuration"})
public class ApplicationBootstrap {

    public static void main(String[] args) {
		SpringApplication.run(ApplicationBootstrap.class, args);
	}
}
