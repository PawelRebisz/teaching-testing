package com.redbull.teaching.testing.configuration;

import com.redbull.teaching.testing.external.JmsDrivenReservationService;
import com.redbull.teaching.testing.external.RestDrivenReservationService;
import com.redbull.teaching.testing.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.jms.ConnectionFactory;

@Configuration
@EnableJms
public class JmsDrivenReservationServiceConfiguration {

    @Autowired
    private ConnectionFactory connectionFactory;

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setConcurrency("1-1");
        return factory;
    }

    @Bean
    public JmsDrivenReservationService jmsDrivenReservationService(ReservationService reservationService) {
        return new JmsDrivenReservationService(reservationService);
    }
}
