package com.redbull.teaching.testing.configuration;

import com.redbull.teaching.testing.external.RestDrivenReservationService;
import com.redbull.teaching.testing.service.ReservationService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class RestDrivenReservationServiceConfiguration {

    @Bean
    public RestDrivenReservationService restDrivenReservationService(ReservationService reservationService) {
        return new RestDrivenReservationService(reservationService);
    }
}
