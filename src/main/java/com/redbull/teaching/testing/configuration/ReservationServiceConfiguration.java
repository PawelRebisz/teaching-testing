package com.redbull.teaching.testing.configuration;

import com.redbull.teaching.testing.repository.DefaultReservationRepository;
import com.redbull.teaching.testing.repository.ReservationRepository;
import com.redbull.teaching.testing.service.DefaultReservationService;
import com.redbull.teaching.testing.service.InvoiceService;
import com.redbull.teaching.testing.service.ReservationService;
import com.redbull.teaching.testing.service.RoomAvailability;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.nio.file.Paths;

@Configuration
public class ReservationServiceConfiguration {

    @Autowired
    private DataSource dataSource;

    @Bean
    public ReservationRepository reservationRepository() {
        return new DefaultReservationRepository(dataSource);
    }

    @Bean
    public RoomAvailability roomAvailability() {
        return new RoomAvailability(1, 2, 3, 4, 5);
    }

    @Bean
    public InvoiceService invoiceService(@Value("${application.service.invoice.path}") String path) {
        return new InvoiceService(Paths.get(path).toAbsolutePath());
    }

    @Bean
    public ReservationService reservationService(ReservationRepository reservationRepository,
                                                 RoomAvailability roomAvailability, InvoiceService invoiceService) {
        return new DefaultReservationService(reservationRepository, roomAvailability, invoiceService);
    }
}
