package com.redbull.teaching.testing.configuration;

import lombok.extern.java.Log;
import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;

@Log
@Configuration
public class ExposingInmemoryDatabaseOverTcpConfiguration {

    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server inMemoryH2DatabaseaServer() throws SQLException {
        log.info("Attempting to expose h2 inmemory database over tcp, under url: 'jdbc:h2:tcp://localhost:9090/mem:testdb'.");
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9090");
    }
}
