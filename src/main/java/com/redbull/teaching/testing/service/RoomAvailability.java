package com.redbull.teaching.testing.service;

import com.redbull.teaching.testing.domain.ReservationRefusedException;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class RoomAvailability {

    private final Map<LocalDate, Set<Integer>> dateToBookedRooms = new HashMap<LocalDate, Set<Integer>>();
    private final Set<Integer> allRoomNumbers;

    public RoomAvailability(Integer... allRoomNumbers) {
        Set<Integer> rooms = new HashSet<>();
        rooms.addAll(Arrays.asList(allRoomNumbers));
        this.allRoomNumbers = Collections.unmodifiableSet(rooms);
    }

    public int reserveForDate(LocalDate date) throws ReservationRefusedException {
        Set<Integer> alreadyBookedRooms = dateToBookedRooms.getOrDefault(date, new HashSet<>());
        dateToBookedRooms.put(date, alreadyBookedRooms);
        Set<Integer> availableRoomNumbers = this.allRoomNumbers.stream().map(Integer::new).collect(Collectors.toSet());
        availableRoomNumbers.removeAll(alreadyBookedRooms);
        if(availableRoomNumbers.size() == 0) {
            throw new ReservationRefusedException("No available rooms for date " + date.toString() + ".");
        }
        Integer reservedRoomNumber = availableRoomNumbers.iterator().next();
        alreadyBookedRooms.add(reservedRoomNumber);
        return reservedRoomNumber;
    }
}
