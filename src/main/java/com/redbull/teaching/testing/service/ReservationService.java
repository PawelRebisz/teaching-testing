package com.redbull.teaching.testing.service;

import com.redbull.teaching.testing.domain.Person;
import com.redbull.teaching.testing.domain.Reservation;
import com.redbull.teaching.testing.domain.ReservationRefusedException;

import java.io.IOException;
import java.time.LocalDate;

public interface ReservationService {
    Reservation makeReservation(Person person, LocalDate date) throws ReservationRefusedException, IOException;
}
