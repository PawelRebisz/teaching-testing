package com.redbull.teaching.testing.service;

import com.redbull.teaching.testing.domain.Person;
import com.redbull.teaching.testing.domain.Reservation;
import com.redbull.teaching.testing.domain.ReservationRefusedException;
import com.redbull.teaching.testing.repository.ReservationRepository;

import java.time.LocalDate;

public class DefaultReservationService implements ReservationService {

    private final ReservationRepository reservationRepository;
    private final RoomAvailability roomAvailability;
    private final InvoiceService invoiceService;

    public DefaultReservationService(ReservationRepository reservationRepository, RoomAvailability roomAvailability, InvoiceService invoiceService) {
        this.reservationRepository = reservationRepository;
        this.roomAvailability = roomAvailability;
        this.invoiceService = invoiceService;
    }

    public Reservation makeReservation(Person person, LocalDate date) throws ReservationRefusedException {
        int roomNumber = roomAvailability.reserveForDate(date);
        Reservation reservation = new Reservation(person, roomNumber, date);
        reservationRepository.acceptReservation(reservation);
        invoiceService.generateInvoice(reservation.getId());
        return reservation;
    }
}
