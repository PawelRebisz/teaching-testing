package com.redbull.teaching.testing.service;

import lombok.extern.java.Log;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Log
public class InvoiceService {

    private final ExecutorService executorService = Executors.newFixedThreadPool(1);
    private final Path invoiceDirectory;

    public InvoiceService(Path invoiceDirectory) {
        log.info("Invoice directory will be set to '" + invoiceDirectory.toString() + "'.");
        this.invoiceDirectory = invoiceDirectory;
        try {
            Files.createDirectories(invoiceDirectory);
        } catch (IOException exception) {
            throw new RuntimeException("Failed to create folder '" + invoiceDirectory + "' for invoices", exception);
        }
    }

    public void generateInvoice(UUID reservationId)  {
        Runnable generationOfInvoice = () -> {
            try {
                heavyComputingAndContentGeneration();
                Path invoicePath = invoiceDirectory.resolve(reservationId + ".txt");
                Files.createFile(invoicePath);
            } catch (Exception exception) {
                throw new RuntimeException(exception);
            }
        };
        executorService.execute(generationOfInvoice);
    }

    private void heavyComputingAndContentGeneration() throws InterruptedException {
        for(int i = 0; i < 1000; i++) {
            Thread.sleep(1);
        }
    }
}
