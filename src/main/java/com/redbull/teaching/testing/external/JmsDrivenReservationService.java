package com.redbull.teaching.testing.external;

import com.redbull.teaching.testing.domain.Person;
import com.redbull.teaching.testing.domain.ReservationRefusedException;
import com.redbull.teaching.testing.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Controller;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import java.io.IOException;
import java.time.LocalDate;

@Controller
public class JmsDrivenReservationService {

    private final ReservationService reservationService;

    @Autowired
    public JmsDrivenReservationService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @JmsListener(destination = "reservations")
    public void makeReservation(Message message) throws IOException, ReservationRefusedException, JMSException {
        MapMessage mapMessage = (MapMessage) message;
        Person person = new Person(
            mapMessage.getString("name"),
            mapMessage.getString("surname"),
            mapMessage.getInt("age")
        );
        LocalDate date = LocalDate.parse(mapMessage.getString("date"));
        reservationService.makeReservation(person, date);
    }
}