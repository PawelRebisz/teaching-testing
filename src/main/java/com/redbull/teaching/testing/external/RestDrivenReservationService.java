package com.redbull.teaching.testing.external;

import com.redbull.teaching.testing.domain.Person;
import com.redbull.teaching.testing.domain.Reservation;
import com.redbull.teaching.testing.domain.ReservationRefusedException;
import com.redbull.teaching.testing.service.ReservationService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Log
@Controller
public class RestDrivenReservationService {

    private final ReservationService reservationService;

    @Autowired
    public RestDrivenReservationService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @RequestMapping(value = "/reservations/",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> makeReservation(@RequestBody Map<String, Object> payload) throws IOException, ReservationRefusedException {
        Person person = new Person(
            (String) payload.get("name"),
            (String) payload.get("surname"),
            (Integer) payload.get("age")
        );
        LocalDate date = LocalDate.parse((String) payload.get("date"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        log.info("Received reservation for " + person.toJson() + " for " + payload.get("date"));
        Reservation reservation = reservationService.makeReservation(person, date);
        Map<String, Object> response = new HashMap<>();
        response.put("reservationId", reservation.getId());
        return response;
    }
}