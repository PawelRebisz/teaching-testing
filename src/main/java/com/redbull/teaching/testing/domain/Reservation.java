package com.redbull.teaching.testing.domain;

import lombok.Getter;

import java.time.LocalDate;
import java.util.UUID;

@Getter
public class Reservation {

    private final UUID id;
    private final Person person;
    private final int roomNumber;
    private final LocalDate date;
    private final int price;

    public Reservation(Person person, int roomNumber, LocalDate date) {
        this.id = UUID.randomUUID();
        this.person = person;
        this.roomNumber = roomNumber;
        this.date = date;
        if (person.getAge() > 60) {
            this.price = 150;
        } else if (person.getAge() > 18) {
            this.price = 250;
        } else {
            this.price = 100;
        }
    }
}
