package com.redbull.teaching.testing.domain;

public class ReservationRefusedException extends Throwable {

    public ReservationRefusedException(String message) {
        super(message);
    }
}
