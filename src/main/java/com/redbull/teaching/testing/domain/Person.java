package com.redbull.teaching.testing.domain;

import lombok.AllArgsConstructor;
import org.json.JSONObject;

@AllArgsConstructor
public class Person {

    private final String name;
    private final String surname;
    private final int age;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("name", name);
        json.put("surname", surname);
        json.put("age", age);
        return json;
    }
}
