package com.redbull.teaching.testing.repository;

import com.redbull.teaching.testing.domain.Reservation;

public interface ReservationRepository {
    void acceptReservation(Reservation reservation);
}
