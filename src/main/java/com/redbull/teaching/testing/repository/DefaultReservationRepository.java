package com.redbull.teaching.testing.repository;

import com.redbull.teaching.testing.domain.Reservation;
import lombok.extern.java.Log;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Date;

@Log
public class DefaultReservationRepository implements ReservationRepository {

    private final JdbcTemplate jdbcTemplate;

    public DefaultReservationRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void acceptReservation(Reservation reservation) {
        String person = reservation.getPerson().getName() + " " + reservation.getPerson().getSurname();
        this.jdbcTemplate.update(
            "insert into reservations (id, room_number, reservation_on, reservation_for, price) values (?,?,?,?,?)",
            reservation.getId(), reservation.getRoomNumber(), Date.valueOf(reservation.getDate()), person, reservation.getPrice()
        );
        log.info("Inserted reservation for " + person + " with id: " + reservation.getId());
    }
}
